

function cleanup_database_galera {
    cleanup_database_mysql
}

function recreate_database_galera {
    local db=$1
    mysql -u$DATABASE_USER -p$DATABASE_PASSWORD -h$MYSQL_HOST -P$MYSQL_PORT -e "DROP DATABASE IF EXISTS $db;"
    mysql -u$DATABASE_USER -p$DATABASE_PASSWORD -h$MYSQL_HOST -P$MYSQL_PORT -e "CREATE DATABASE $db CHARACTER SET utf8;"
}

function configure_database_galera {
echo;
}

function install_database_galera {
    install_package mariadb
}


function install_database_python_galera {
    # Install Python client module
    pip_install_gr MySQL-python
    ADDITIONAL_VENV_PACKAGES+=",MySQL-python"
}

function database_connection_url_galera {
    local db=$1
    echo "$BASE_SQL_CONN/$db?charset=utf8"
}


