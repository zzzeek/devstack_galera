===============
Devstack galera
===============


Config in local.conf (will improve this)::

    enable_plugin galera_devstack http://bitbucket.org/zzzeek/devstack_galera.git
    disable_service mysql
    enable_service galera

    DATABASE_USER=openstack
    DATABASE_PASSWORD=sa
    DATABASE_HOST=192.168.1.210:3456
    DATABASE_TYPE=mysql
    MYSQL_HOST=192.168.1.210
    MYSQL_PORT=3456

